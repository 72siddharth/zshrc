# Prompt Customization
function git_check() {
	if git rev-parse --abbrev-ref 2> /dev/null ; then
		echo "%F{blue}git:(%f%F{yellow}%B$(git branch --show-current)%b%f%F{blue})%f"
	fi
}
function zle-line-init zle-keymap-select {
    ARW="${${KEYMAP/vicmd/ᛝ}/(main|viins)/ᛟ}"
	GIT_BR=$(git_check)
	RPROMPT="${GIT_BR}"
	PROMPT="%F{yellow}%~%f %B%F{%(0?.blue.red)}$ARW%f%b "
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select

# Adding useful path
source $HOME/some-path.sh

# Qt5ct
export QT_QPA_PLATFORMTHEME=qt5ct

# Enable colors
autoload -U colors && colors

# Pamcan Hook
zshcache_time="$(date +%s%N)"
autoload -Uz add-zsh-hook
rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}
add-zsh-hook -Uz precmd rehash_precmd

# Autocomplete
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
_comp_options+=(globdots)
setopt COMPLETE_ALIASES

# Vi mode
bindkey -v
export  KEYTIMEOUT=1

# Vim keys for autocomplete menu
bindkey -M menuselect 'h' 	vi-backward-char
bindkey -M menuselect 'j' 	vi-down-line-or-history
bindkey -M menuselect 'k' 	vi-up-line-or-history
bindkey -M menuselect 'l' 	vi-forward-char
bindkey -M menuselect 'left' 	vi-backward-char
bindkey -M menuselect 'down' 	vi-down-line-or-history
bindkey -M menuselect 'up' 	vi-up-line-or-history
bindkey -M menuselect 'right' 	vi-forward-char

# Backspace in insert after switching modes
bindkey "^?" backward-delete-char

# Edit line in nvim at <C-v>
autoload -U  edit-command-line
zle      -N  edit-command-line
bindkey '^v' edit-command-line

# History
HISTFILE=~/.config/zsh/histfile
HISTSIZE=10000
SAVEHIST=10000

# Aliases
alias l=lsd
alias la="lsd -a"
alias wget="wget --hsts-file=~/.cache/wget-hsts"
alias passzip="$HOME/.config/zsh/passer.sh"
alias vim=nvim
alias v=nvim
alias pac="sudo pacman"
alias cls="clear"
alias Sys="sudo systemctl"
alias sys="systemctl"
alias YTplaylist="youtube-dl -f \"bestaudio\" --continue --no-overwrites --ignore-errors --extract-audio --audio-format mp3 -o \"%(title)s.%(ext)s\""
alias Crun="cargo run"
alias Cbuild="cargo build"

# Zoxide init
eval "$(zoxide init zsh)"

# Syntax Highlighting & Autosuggestion & Repo Check
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/doc/pkgfile/command-not-found.zsh 2>/dev/null

# O Moon God, fetch us the divine info
moonfetch
